/// <reference types="cypress" />


const sampleForm = require('../fixtures/sampleForm.json')
const API_URL = Cypress.env('API_BASE_URL')
const auth = `Bearer ${Cypress.env('AUTH_TOKEN')}`

describe('API Tests', () => {

    it('Return my user information', () => {
        cy.request({
            method: 'GET',
            url: `${API_URL}me`,
            failOnStatusCode: false,
            headers: { authorization : auth }
            //should é melhor pois tem retentativa de submeter a requisição, enquanto o then não tem
        }).should((response) => {
            console.log(response)
            expect(response.status).to.eq(200)
            expect(response.body.alias).to.eq('Bruno CK')
        })
    })

    it('Return all forms details', () => {
        cy.request({
            method: 'GET',
            url: `${API_URL}forms`,
            headers: { authorization : auth }
        }).should((response) => {
            //console.log(response)
        })
    })

    it('Return an specific form responses', () => {
        cy.request({
            method: 'GET',
            url: `${API_URL}forms/${Cypress.env('formId')}/responses`,
            headers: { authorization : auth }
        }).should((response) => {
            console.log(response.body)
            expect(response.body.total_items).to.eq(1)
            expect(response.body.items.length).to.eq(1)
        })
    })

    describe('', () => {
        beforeEach(() => {
            cy.request({
                method: 'GET',
                url: `${API_URL}forms`,
                headers: { authorization : auth }
            }).should(({status, body}) => {
                expect(status).to.eq(200)
                body.items.forEach(item => {
                    if(item.title === sampleForm.title) {
                        cy.request({
                            method: 'DELETE',
                            url: `${API_URL}forms/${item.id}`,
                            headers: { authorization : auth }
                        })
                    }
                })
            })
        })

        it('Creates a sample form', () => {
            cy.request({
                method: 'POST',
                url: `${API_URL}forms`,
                headers: { authorization : auth },
                body: sampleForm
            })
        })

        it.only('Log on WTP', () => {
            cy.request({
                method: 'POST',
                url: "https://test.wisertp.com/api/v1/teachers/auth/login",
                headers: { 
                    authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImViNjY3MjlmLWNlZTYtNDgyNS04M2FkLTAyMDU1NTA2NTk0YyIsImlhdCI6MTY1MjgyMzk0NSwiZXhwIjoxNjUyODI0ODQ1fQ.jOwpgipstX6oLMr8t1mxul0O8XVRecOtTIGqHsIymi8",
                    //"Client-device" : "browser"
                },
                body: {
                    email: "test-automated.coordenador007@wisereducacao.com",
                    password: "Test@2220",
                }
            })
        })
    })
    

})
